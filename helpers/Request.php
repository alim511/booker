<?php

class Request {
    private $params = array();
    public function __construct() {
        if ($_SERVER['REQUEST_URI'] != '/') {
            try {
                $url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

                $uri_parts = explode('/', trim($url_path, ' /'));

//                if (in_array('index.php', $uri_parts)) {
//                    $count_uri_parts = count($uri_parts);
//                    for ($i = 0; $i < $count_uri_parts; $i++) {
//                        $urlKey = $uri_parts[0];
//                        array_shift($uri_parts);
//                        if ('index.php' == $urlKey) {
//                            break;
//                        }
//                    }
//                }
                if (count($uri_parts) % 2) {
                    throw new Exception();
                }

                $this->params['controller'] = array_shift($uri_parts);
                $this->params['action'] = array_shift($uri_parts);

                for ($i=0; $i < count($uri_parts); $i++) {
                    $this->params[$uri_parts[$i]] = $uri_parts[++$i];
                }
            } catch (Exception $e) {
                $this->params = array('controller' => 'index', 'action' => 'error');
            }
        }
    }

    public function getParam($key) {
        $result = false;
        if (!empty($this->params[$key])) {
            $result = $this->params[$key];
        }
        return $result;
    }
}