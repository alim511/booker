<?php

class Routes {

    public $controller = 'index';
    public $action = 'home';

    public function __construct() {
        $request = new Request();
        if (!empty($request->getParam('controller'))) {
            $this->controller = $request->getParam('controller');
        }
        if (!empty($request->getParam('action'))) {
            $this->action = $request->getParam('action');
        }
    }

    public function run() {
        if (class_exists($this->controller . 'Controller')
            && method_exists($this->controller . 'Controller', $this->action)) {
            $controller = $this->controller . 'Controller';
            $this->call($controller, $this->action);
        } else {
            // throw new exeption
            die('Wrong URL');
        }
    }

    private function call($controller, $action) {
        $controllerObj = new $controller;
        $controllerObj->{$action}();
    }
}