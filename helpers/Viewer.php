<?php

class Viewer
{
    public $layout = false;
    public $layoutParams = array();
    private $template;
    private $page = '';

    public function setTemplate ($template)
    {
        $routes = new Routes();
        $filename = 'views/' . $routes->controller . '/' . $template . '.php';
        if ( file_exists($filename) )
        {
            $this->template = file_get_contents($filename);
        }
        else
        {
            throw new Exception("Template not exist");
        }
    }

    public function render($template = '', $params = array())
    {
        if (!empty($this->layout)) {
            $this->addLayout();
        }

        if (!empty($template)) {
            $this->setTemplate($template);
        } else {
            throw new Exception("Template must not be empty");
        }

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $this->template = str_replace('{{' . $key . '}}', $value, $this->template);
            }
        }

        if (!empty($this->page)) {
            $this->page = str_replace('{{CONTENT}}', $this->template, $this->page);
        }
        print_r($this->page);
    }

    private function addLayout() {
        $filename = $this->layout . '.php';
        if ( file_exists('views/' . $filename))
        {
            $this->page = file_get_contents('views/' . $filename);
            if (!empty($this->layoutParams)) {
                foreach ($this->layoutParams as $key => $value) {
                    $this->page = str_replace('{{' . $key . '}}', $value, $this->page);
                }
            }
        }
        else
        {
            throw new Exception("Layout no exist");
        }
    }

}

?>
