<?php

/********************** Routes **********************************/
define('HOST', 'http://booker.loc/');

/********************** Connect DB ******************************/

define('DB_HOST', 'localhost');
define('DB_NAME', 'booker');
define('DB_USER', 'root');
define('DB_PASS', '');

/********************** Autoload Pathes ***************************/

define('MPATH', 'models/');
define('VPATH', 'views/');
define('CPATH', 'controllers/');
define('LPATH', 'lib/');
define('HPATH', 'helpers/');
define('ROOT', __DIR__);

?>