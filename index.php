<?php

require_once 'config.php';
require_once 'lib/Loader.php';

try {
    $loader = new Loader();

    $routes = new Routes();
    $routes->run();
} catch (Exception $e) {
    echo 'Error: ' .$e->getMessage();
}

?>