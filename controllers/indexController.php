<?php

class indexController {
    public $viewer = false;

    public function __construct() {
        $this->viewer = new Viewer();
        $this->viewer->layout = 'layout';
        $this->viewer->layoutParams = array('HOST' => HOST);
    }

    public function home() {
        $this->viewer->render('home');
    }

    public function error() {
        require_once('error');
    }
}