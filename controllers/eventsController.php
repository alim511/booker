<?php
class eventsController {
    public function __construct() {
        $this->viewer = new Viewer();
        $this->viewer->layout = 'layout';
        $this->viewer->layoutParams = array('HOST' => HOST);
    }

    public function event() {
        $this->viewer->render('events');
    }

    public function addEvent() {
        $this->viewer->render('addevent');
    }

    public function ajaxAdd() {
        $this->viewer->layout = false;

        if (isset($_POST['addevent'])) {
            $event = new Event();
            $event->room_id            = filter_var($_POST["room_id"], FILTER_SANITIZE_STRING);
            $event->user               = filter_var($_POST["user"], FILTER_SANITIZE_STRING);
            $event->date               = filter_var($_POST["date"], FILTER_SANITIZE_STRING);
            $event->start              = filter_var($_POST["start"], FILTER_SANITIZE_STRING);
            $event->end                = filter_var($_POST["end"], FILTER_SANITIZE_STRING);
            $event->description        =  filter_var($_POST["description"], FILTER_SANITIZE_STRING);
            $event->reccuring          =  filter_var($_POST["reccuring"], FILTER_SANITIZE_STRING);
            $event->reccuring_info     =  filter_var($_POST["reccuring_info"], FILTER_SANITIZE_STRING);
            $event->reccuring_duration =  filter_var($_POST["reccuring_duration"], FILTER_SANITIZE_STRING);
            $event->save();
        }
    }

    public function ajaxGetAllEvents() {
        $this->viewer->layout = false;

        $event = new Event();
        $event -> getEvents();
    }
}