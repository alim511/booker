<?php

class usersController {
    public function __construct() {
        $this->viewer = new Viewer();
        $this->viewer->layout = 'layout';
        $this->viewer->layoutParams = array('HOST' => HOST);
    }

    public function users() {
        $this->viewer->render('users');
    }

    public function user() {
        $this->viewer->render('user');
    }

    public function edit() {
        $this->viewer->render('edituser');
    }

    public function add() {
        $this->viewer->render('adduser');
    }

    public function delete() {
        $this->viewer->render('deleteuser');
    }

    public function ajaxAdd() {
        $this->viewer->layout = false;

        if (isset($_POST['adduser'])) {
            $user = new User();
            $user->name     = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
            $user->email    = filter_var($_POST["email"], FILTER_SANITIZE_STRING);
            $user->password = filter_var($_POST["password"], FILTER_SANITIZE_STRING);
            $user->role_id  = filter_var($_POST["role_id"], FILTER_SANITIZE_STRING);
            $user->save();
        }
    }

    public function ajaxEdit() {
        $this->viewer->layout = false;

        if (isset($_POST['edituser'])) {
            $user = new User();
            $user->id       = filter_var($_POST["id"], FILTER_SANITIZE_STRING);
            $user->name     = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
            $user->email    = filter_var($_POST["email"], FILTER_SANITIZE_STRING);
            $user->password = filter_var($_POST["password"], FILTER_SANITIZE_STRING);
            $user->role_id  = filter_var($_POST["role_id"], FILTER_SANITIZE_STRING);
            $user->save();
        }
    }
}