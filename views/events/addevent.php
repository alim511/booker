<?php

$user = new User();
$users = $user->getUsers();

?>

<h1>Add event page</h1>

<form class="add-event-form form">
    <div class="fields-wrapper">
        <label class="form-label">Booked for: </label>
        <select class="users-list">
            <?php
            foreach ($users as $user) {
                echo '<option value="' . echo $user['name'] . '">' . echo $user['name'] . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="fields-wrapper">
        <label class="form-label">I would like to book this meting: </label>
        <select class="month"></select>
        <select class="day"></select>
        <select class="year"></select>
    </div>
    <div class="fields-wrapper">
        <label class="form-label">Specify what  the time and end of the meeting(This will be what people see on the calendar.)</label>
        <div>
            <select class="start-hours"></select>
            <select class="start-minutes"></select>
            <select class="start-am-pm">
                <option value="AM">AM</option>
                <option value="PM">PM</option>
            </select>
        </div>
        <div>
            <select class="end-hours"></select>
            <select class="end-minutes"></select>
            <select class="end-am-pm">
                <option value="AM">AM</option>
                <option value="PM">PM</option>
            </select>
        </div>
    </div>
    <div class="fields-wrapper">
        <label class="form-label">Enter the specifics for the meeting(This will be what people see when they click on an event link.)</label>
        <textarea class="form-textarea description" name="description"></textarea>
    </div>
    <div class="fields-wrapper">
        <label class="form-label">Is this going to be reccuring event?</label>
        <div class="radio-wrapper">
            <input type="radio" name="reccuring" value="0" checked> No
        </div>
        <div class="radio-wrapper">
            <input type="radio" name="reccuring" value="1"> Yes
        </div>
    </div>
    <div class="fields-wrapper">
        <label class="form-label">If it's reccuring, specify weekly, bi-weekly or monthly.</label>
        <div class="radio-wrapper">
            <input type="radio" name="reccuring_info" value="weekly" checked> weekly
        </div>
        <div class="radio-wrapper">
            <input type="radio" name="reccuring_info" value="bi-weekly"> bi-weekly
        </div>
        <div class="radio-wrapper">
            <input type="radio" name="reccuring_info" value="monthly"> monthly
        </div>
    </div>
    <div class="fields-wrapper">
        <label class="form-label">If weekly of bi-weekly, specify the number of weeks for it to keep recurring.
        If monthly, specify the number of month. (If you choose "bi-weekly" and put it in odd number of weeks,
            the computer will round down.)</label>
        <input class="form-field" type="text" name="reccuring_duration" placeholder="max 4 weeks">
    </div>
    <button class="add-event-btn form-button">Submit</button>
</form>