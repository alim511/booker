<?php

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    $user = new User();
    $getUser = $user->getUser($id);
} else {
    echo 'Employee id not selected!';
    die();
}

?>
<h1>Employee <?php  echo $getUser[0]['name']; ?></h1>
<table border="1" width="100%">
    <tr>
        <td>name</td>
        <td>email</td>
        <td>role id</td>
        <td>password</td>
        <td>actions</td>
    </tr>
    <tr>
        <?php
            foreach ($getUser as $user) {
                echo '<td>' . $user['name'] . '</td>' .
                     '<td>' . $user['email'] . '</td>' .
                     '<td>' . $user['role_id'] . '</td>' .
                     '<td>' . $user['password'] . '</td>';
            }
        ?>
        <td>
            <a href="{{HOST}}users/edit/id/<?php echo $getUser[0]['id']; ?>">edit</a>
        </td>
    </tr>
</table>