<?php

$user = new User();
$getUsers = $user->getUsers();

if (isset($_GET['delete'])) {
    $user->delete($_GET['delete']);
}

?>
<h1>List of the employees</h1>
<table border="1" width="100%">
    <tr>
        <td>Employee list</td>
        <td>Actions</td>
    </tr>
    <?php
    foreach ($getUsers as $users) {
        echo '<tr><td><a href="mailto:'. $users['email'] .'">' . $users['name'] . '</a></td>'
             . '<td><a href="'.{{HOST}}.'users/edit/id/' . $users['id'] . '">edit</a>&nbsp;'
             . '<a href="#" class="delete-users" data-id="' . $users['id'] . '">delete</a></td></tr>';
    }
    ?>
</table>
<a href="?c=users&a=add">add employee</a>

<script>
    $('.delete-users').click(function (e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        var confirmWindow = confirm("you sure you want to delete this contact?");
        if (confirmWindow == true) {
            location.href = '{{HOST}}users/users/delete/'+id;
        }
    })
</script>