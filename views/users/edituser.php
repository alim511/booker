<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    $user = new User();
    $getUser = $user->getUser($id);
} else {
    echo 'User id not selected!';
    die();
}
?>

<h1>Edit employee <?php  echo $getUser[0]['name']; ?></h1>

<form class="edit-employee-form form">
    <input type="hidden" name="id" placeholder="<?php echo $getUser[0]['id'] ?>">
    <div class="fields-wrapper">
        <label class="form-label">Name *</label>
        <input class="form-field" type="text" name="name" placeholder="<?php echo $getUser[0]['name'] ?>">
    </div>
    <div class="fields-wrapper">
        <label class="form-label">Email *</label>
        <input class="form-field" type="text" name="email" placeholder="<?php echo $getUser[0]['email'] ?>">
    </div>
    <div class="fields-wrapper">
        <label class="form-label">Role ID *</label>
        <input class="form-field" type="text" name="role_id" placeholder="<?php echo $getUser[0]['role_id'] ?>">
    </div>
    <div class="fields-wrapper">
        <label class="form-label">Password *</label>
        <input class="form-field" type="text" name="password" placeholder="<?php echo $getUser[0]['password'] ?>">
    </div>
    <button class="edit-employee-btn form-button">Save</button>
</form>