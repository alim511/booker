
<h1>Add Employee</h1>

<form class="add-employee-form form">
    <div class="fields-wrapper">
        <label class="form-label">Name *</label>
        <input class="form-field" type="text" name="name">
    </div>
    <div class="fields-wrapper">
        <label class="form-label">Email *</label>
        <input class="form-field" type="text" name="email">
    </div>
    <div class="fields-wrapper">
        <label class="form-label">Role ID *</label>
        <input class="form-field" type="text" name="role_id">
    </div>
    <div class="fields-wrapper">
        <label class="form-label">Password *</label>
        <input class="form-field" type="text" name="password">
    </div>
    <button class="add-employee-btn form-button">Add</button>
</form>