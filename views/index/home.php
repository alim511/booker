
<h1>Home page</h1>
<div class="pagination-wrapper">
    <div class="prev"><</div>
    <div class="next">></div>
</div>
<div class="calendar">
    <h2 class="month-name"></h2>
    <h2 class="year"></h2>
    <div class="dotw"></div>
    <div class="days-wrapper"></div>
</div>

<script src="js/calendar.js"></script>