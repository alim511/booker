<DOCTYPE html>
<html>
    <head>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="/js/script.js"></script>
        <link type="text/css" href="/css/style.css" rel="stylesheet"></link>
    </head>
    <body>
        <header>
            <div class="grid">
                <nav>
                    <a href="{{HOST}}">Home</a>
                    <a href="{{HOST}}users/users">Employee List</a>
                    <a href="{{HOST}}users/add">Add Employee</a>
                </nav>
            </div>
        </header>
        <article>
            <div class="grid">
                {{CONTENT}}
            </div>
        </article>
        <footer>
            <div class="grid">
                &copy; Alimov Dmitriy
            </div>
        </footer>
    </body>
</html>