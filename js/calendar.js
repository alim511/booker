$(document).ready(function () {

    var userLang = navigator.language || navigator.userLanguage;
    var date = new Date();
        day = date.getDate();
        month = date.getMonth() + 1;
        year = date.getFullYear();

    /* check users locale */
    if (userLang == 'en-US') {
        days = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
    } else {
        days = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
    }

    /* render month name */
    function renderMonthYearName() {
        monthName = new Date(year, month - 1).toLocaleString(userLang, { month: "long" });
        $('.month-name').append(monthName);
        $('.year').append(year);
    }

    /* render names of the days */
    function renderDaysNames() {
        daysLength = days.length;
        var i = 0;
        while (i <= daysLength - 1) {
            $('.dotw').append('<div class="name-of-the-day">' + days[i] +'</div>');
            i++;
        }
    }

    /* check if today is current day */
    function currentDay() {
        if (day == date.getDate() && month == date.getMonth() + 1 && year == date.getFullYear()) {
            return date.getDate();
        } else {
            return false;
        }
    }

    /* render days */
    function renderDays(month, year) {
        var daysInMonth = new Date(year, month, 0).getDate();
            getFirstDayOfMonth = new Date(year, month - 1, 1).getDay();
            i = 1;
            j = 1;

        while (i <= daysInMonth) {
            if (i == currentDay()) {
                $('.days-wrapper').append('<div class="day __current" data-date="'+year+-+i+-+month+'"><span class="digit">' + i +'</span></div>');
            } else {
                $('.days-wrapper').append('<div class="day" data-date="'+year+-+i+-+month+'"><span class="digit">' + i +'</span></div>');
            }
            i++;
        }
        while (j <= getFirstDayOfMonth - 1) {
            $('.days-wrapper').prepend('<div class="day-blank"></div>');
            j++;
        }

    }

    /* paginations */
    $('.prev').click(function() {
        month = month - 1;
        if (month < 1) {
            month = 12;
            year = year - 1;
        }
        renderCalendar();
    })

    $('.next').click(function() {
        month = month + 1;
        if (month > 12) {
            month = 1;
            year = year + 1;
        }
        renderCalendar();
    })

    /* render/re-render full callendar */
    function renderCalendar() {
        /* clean HTML */
        $('.year, .month-name, .dotw, .days-wrapper').empty();
        renderMonthYearName();
        renderDaysNames();
        renderDays(month, year);
    }

    renderCalendar();
});