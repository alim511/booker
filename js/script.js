$(document).ready(function() {

    function ajax(data, url) {
        $.ajax({
            type: 'POST',
            url: url,
            cache: 'false',
            data: data,
            success: function(data){
                obj = $.parseJSON(data);
                console.log(obj);
            }
        });
    };

    /* add new user */
    $('.add-employee-btn').click(function(e){
        e.preventDefault();
        data = {
            'adduser': 'adduser',
            'name': $('input[name*="name"]').val(),
            'email': $('input[name*="email"]').val(),
            'role_id': $('input[name*="role_id"]').val(),
            'password': $('input[name*="password"]').val()
        }
        url = '/users/ajaxAdd';

        ajax(data, url);
    });

    /* edit user */
    $('.edit-employee-btn').click(function(e){
        e.preventDefault();
        data = {
            'edituser': 'edituser',
            'id': $('input[name*="id"]').val(),
            'name': $('input[name*="name"]').val(),
            'email': $('input[name*="email"]').val(),
            'role_id': $('input[name*="role_id"]').val(),
            'password': $('input[name*="password"]').val()
        }
        url = '/users/ajaxEdit';

        ajax(data, url);
    });

    /* add new event */
    $('.add-event-btn').click(function(e){
        e.preventDefault();
        data = {
            'addevent': 'addevent',
            'room_id': '2',
            'user': $('.users_list option:selected').text(),
            'date': $('.month option:selected').text() + - + $('.day option:selected').text() + - + $('.year option:selected').text(),
            'start': $('.start-hours option:selected').text() + $('.start-minutes option:selected').text() + $('.start-am-pm option:selected').text(),
            'end': $('.end-hours option:selected').text() + $('.end-minutes option:selected').text() + $('.end-am-pm option:selected').text(),
            'description': $('textarea[name*="description"]').val(),
            'reccuring': $('input[name=reccuring]:checked').val(),
            'reccuring_info': $('input[name=reccuring_info]:checked').val(),
            'reccuring_duration': $('input[name*="reccuring_duration"]').val()
        }
        url = '/events/ajaxAdd';

        ajax(data, url);
    });

    /* get all events on the home page*/
    function getAllEvents() {
        $.getJSON('events/ajaxGetAllEvents', function(data) {
            eventsArr = $.map(data, function(value, index) {
                return [value];
            });
            var items = [];
            i=0;

            /* parse array */
            while (i < eventsArr.length) {
                $.each(eventsArr[i], function(key, val){
                    items.push('<li id="' + key + '">' + val + '</li>');
                });
                $('<ul/>', {
                    'class': 'my-new-list',
                    html: items.join('')
                }).appendTo('body');
                i++;
            }
            if ( $.inArray($('.day').attr('data-date'), eventsArr ) > -1 ) {
                console.log($(this));
            }
            time = $('.day').attr('data-date');
            //$('.day').each(function(index) {
            //    console.log($( this ).attr('data-date'));
            //})
        });
    }
    getAllEvents();

})