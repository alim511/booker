<?php

class Loader
{

    /**
     * Controller Directory Path
     *
     * @var Array
     * @access protected
     */
    protected $_controllerDirectoryPath = array();

    /**
     * Model Directory Path
     *
     * @var Array
     * @access protected
     */
    protected $_modelDirectoryPath = array();

    /**
     * Library Directory Path
     *
     * @var Array
     * @access protected
     */
    protected $_libraryDirectoryPath = array();


    /**
     * Constructor
     * Constant contain my full path to Model, View, Controllers and Lobrary-
     * Direcories.
     *
     * @Constant MPATH,VPATH,CPATH,LPATH
     */

    public function __construct()
    {
        $this->modelDirectoryPath      = MPATH;
        $this->viewDirectoryPath        = VPATH;
        $this->controllerDirectoryPath = CPATH;
        $this->libraryDirectoryPath     = LPATH;
        $this->helperDirectoryPath     = HPATH;

        spl_autoload_register(array($this,'load_controller'));
        spl_autoload_register(array($this,'load_model'));
        spl_autoload_register(array($this,'load_library'));
        spl_autoload_register(array($this,'load_helper'));
    }

    /**
     * Autoload Controller class
     *
     * @param  string $class
     * @return object
     */

    public function load_controller($controller)
    {
        if ($controller) {
            set_include_path($this->controllerDirectoryPath);
            spl_autoload_extensions('.php');
            spl_autoload($controller);
        }
    }


    /**
     * Autoload Model class
     *
     * @param  string $class
     * @return object
     */

    public function load_model($model)
    {
        if ($model) {
            set_include_path($this->modelDirectoryPath);
            spl_autoload_extensions('.php');
            spl_autoload($model);
        }
    }

    /**
     * Autoload Library class
     *
     * @param  string $class
     * @return object
     */

    public function load_library($library)
    {
        if ($library) {
            set_include_path($this->libraryDirectoryPath);
            spl_autoload_extensions('.php');
            spl_autoload($library);
        }
    }

    /**
     * Autoload Library class
     *
     * @param  string $class
     * @return object
     */

    public function load_helper($helper)
    {
        if ($helper) {
            set_include_path($this->helperDirectoryPath);
            spl_autoload_extensions('.php');
            spl_autoload($helper);
        }
    }
}

?>