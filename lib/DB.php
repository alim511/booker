<?php

class DB {

    protected $db_name = DB_NAME;
    protected $db_user = DB_USER;
    protected $db_pass = DB_PASS;
    protected $db_host = DB_HOST;
    protected static $_instance;
    protected $connect = false;

    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    protected function __construct(){
        $this->connect();
    }

    private function connect() {
        try {
            $this->connect = new PDO("mysql:host=$this->db_host;dbname=$this->db_name", $this->db_user, $this->db_pass);
        } catch (PDOException $e) {
            die("Could not connect to the database $this->db_name :" . $e->getMessage());
        }
    }

    private function __clone(){
    }

    public function getDb() {
        if ($this->connect instanceof PDO) {
            return $this->connect;
        }
    }

    public function close() {
        // close the database connection
        $this->connect = null;
    }
}

?>