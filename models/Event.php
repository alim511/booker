<?php

class Event {

    public $id;
    public $start;
    public $end;
    public $user;
    public $room_id;
    public $description;
    public $date;
    public $reccuring;
    public $reccuring_info;
    public $reccuring_duration;

    public function __construct($id = false) {
        if (!empty($id)) {
            $this->getEvent($id);
        }
    }

    public function getEvent($id) {
        $base = DB::getInstance()->getDb();
        $sql = 'SELECT * FROM events WHERE id = :id';
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':id', $id);

        if ($stmt->execute()) {
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            var_dump($data);
            if (!empty($data[0])) {
                foreach ($data[0] as $k => $value) {
                    $this->{$k} = $value;
                }
            }
        } else {
            echo 'events not exists';
        }
    }

    public function save() {
        if (!empty($this->id)) {
            $this->update();
        } else {
            $this->insert();
        }
    }

    public function getEvents() {

        $base = DB::getInstance()->getDb();

        $qry = 'SELECT * FROM events';
        if ($data = $base->query($qry)) {
            echo json_encode($data->fetchAll(PDO::FETCH_OBJ));
            //var_dump($data->fetchAll(PDO::FETCH_OBJ));
        } else {
            echo 'result empty';
        }
    }

    public function insert() {
        $base = DB::getInstance()->getDb();
        $sql = 'INSERT INTO events (start, end, user, room_id, description, date, reccuring, reccuring_info, reccuring_duration)'
            . ' VALUES (:start, :end, :user, :room_id, :description, :date, :reccuring, :reccuring_info, :reccuring_duration)';
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':start', $this->start);
        $stmt->bindParam(':end', $this->end);
        $stmt->bindParam(':user', $this->user);
        $stmt->bindParam(':room_id', $this->room_id);
        $stmt->bindParam(':description', $this->description);
        $stmt->bindParam(':date', $this->date);
        $stmt->bindParam(':reccuring', $this->reccuring);
        $stmt->bindParam(':reccuring_info', $this->reccuring_info);
        $stmt->bindParam(':reccuring_duration', $this->reccuring_duration);
        $result = $stmt->execute();
        if (!empty($result)) {
            $this->id = $base->lastInsertId();
            echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }

    public function update() {
        $base = DB::getInstance()->getDb();
        $sql = 'UPDATE events SET'
            . ' start=:start, end=:end, user=:user, room_id=:room_id, description=:description, date=:date, reccuring=:reccuring, reccuring_info=:reccuring_info, reccuring_duration=:reccuring_duration'
            . ' WHERE id =:id';
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':start', $this->start);
        $stmt->bindParam(':end', $this->end);
        $stmt->bindParam(':user', $this->user);
        $stmt->bindParam(':room_id', $this->room_id);
        $stmt->bindParam(':description', $this->description);
        $stmt->bindParam(':date', $this->date);
        $stmt->bindParam(':reccuring', $this->reccuring);
        $stmt->bindParam(':reccuring_info', $this->reccuring_info);
        $stmt->bindParam(':reccuring_duration', $this->reccuring_duration);
        $stmt->bindParam(':id', $this->id);
        $result = $stmt->execute();
        if (!empty($result)) {
            echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }

    public function delete($id) {
        $base = DB::getInstance()->getDB();
        $sql = 'DELETE FROM events WHERE id=' . $id;
        echo $sql;
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':id', $id);
        $result = $stmt->execute();
    }
}

?>