<?php

class User {

    public $id;
    public $name;
    public $email;
    public $role_id;
    public $password;

    public function __construct($id = false) {
        if (!empty($id)) {
            $this->getUser($id);
        }
    }

    public function getUser($id) {
        $base = DB::getInstance()->getDb();
        $sql = 'SELECT * FROM users WHERE id = :id';
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':id', $id);

        if ($stmt->execute()) {
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($data[0])) {
                return $data;
//                foreach ($data[0] as $k => $value) {
//                   $this->{$k} = $value;
//                }
            }
        } else {
            echo 'users not exists';
        }
    }

    public function save() {
        if (!empty($this->id)) {
            $this->update();
        } else {
            $this->insert();
        }
    }

    public function getUsers() {

        $base = DB::getInstance()->getDb();

        $qry = 'SELECT * FROM users';
        if ($data = $base->query($qry)) {
            return $data;
            //var_dump($data->fetchAll(PDO::FETCH_OBJ));
        } else {
            echo 'result empty';
        }
    }

    public function insert() {
        $base = DB::getInstance()->getDb();
        $sql = 'INSERT INTO users (name, email, password, role_id)'
            . ' VALUES (:name, :email, :password, :role_id)';
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':role_id', $this->role_id);
        $result = $stmt->execute();
        if (!empty($result)) {
            $this->id = $base->lastInsertId();
            echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }

    public function update() {
        $base = DB::getInstance()->getDb();
        $sql = 'UPDATE users SET'
            . ' name=:name, email=:email, password=:password, role_id=:role_id'
            . ' WHERE id =:id';
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':role_id', $this->role_id);
        $stmt->bindParam(':id', $this->id);
        $result = $stmt->execute();
        if (!empty($result)) {
            echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }

    public function delete($id) {
        $base = DB::getInstance()->getDB();
        $sql = 'DELETE FROM users WHERE id=' . $id;
        echo $sql;
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':id', $id);
        $result = $stmt->execute();
    }
}

?>