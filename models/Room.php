<?php

class Room {

    public $id;
    public $room;

    public function __construct($id = false) {
        if (!empty($id)) {
            $this->getEvent($id);
        }
    }

    public function getRoom($id) {
        $base = DB::getInstance()->getDb();
        $sql = 'SELECT * FROM rooms WHERE id = ' . $id;
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':id', $id);

        if ($stmt->execute()) {
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            var_dump($data);
            if (!empty($data[0])) {
                foreach ($data[0] as $k => $value) {
                    $this->{$k} = $value;
                }
            }
        } else {
            echo 'room not exists';
        }
    }

    public function save() {
        if (!empty($this->id)) {
            $this->update();
        } else {
            $this->insert();
        }
    }

    public function getRooms() {

        $base = DB::getInstance()->getDb();

        $qry = 'SELECT * FROM rooms';
        if ($data = $base->query($qry)) {
            var_dump($data->fetchAll(PDO::FETCH_OBJ));
        } else {
            echo 'result empty';
        }
    }

    public function insert() {
        $base = DB::getInstance()->getDb();
        $sql = 'INSERT INTO rooms (room)'
            . ' VALUES (:room)';
        echo $sql;
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':room', $this->room);
        $result = $stmt->execute();
        if (!empty($result)) {
            $this->id = $base->lastInsertId();
        } else {
            echo 'Event already exist!';
        }
    }

    public function update() {
        $base = DB::getInstance()->getDb();
        $sql = 'UPDATE rooms SET'
            . ' room=:room'
            . ' WHERE id =:id';
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':room', $this->room);
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();
    }

    public function delete($id) {
        $base = DB::getInstance()->getDB();
        $sql = 'DELETE FROM rooms WHERE id=' . $id;
        echo $sql;
        $stmt = $base->prepare($sql);
        $stmt->bindParam(':id', $id);
        $result = $stmt->execute();
    }
}

?>